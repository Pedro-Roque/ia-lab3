from parser import *
import graph as gp
from ve import *
import argparse
import time
start_time = time.time()

parser = argparse.ArgumentParser(usage=__doc__)
parser.add_argument("bn", type=str, default="Test1.bn", help="Bayesian Network file.")
parser.add_argument("inF", type=str, default="Test1.in", help="Query and Evidence file.")
args = parser.parse_args()

bn = parseBN(args.bn)
queryAndEvidence = parseIN(args.inF)
Grafo = gp.graphConstructor()

# Create graph
for var in bn[1]:
	parents = None
	alias = None
	values = None
	cpt = None

	if 'alias' in var:
 		alias = var['alias']

	if 'values' in var:
		values = [x.lower() for x in var['values']]

	for cptVar in bn[0]:
		if cptVar['var'] == var['name'] or cptVar['var'] == var['alias']:
			cpt = cptVar['table']

	Grafo.addNode(var['name'],alias,cpt,values)

# Add relationships: can only be done after graph creation
for var in bn[1]:
	if 'parents' in var:
		for parent in var['parents']:
			Grafo.addEdge(parent,var['name'])
		Grafo.addNode(var['name'],parents=var['parents'])

# Program main
result = varElim(Grafo,queryAndEvidence[0],queryAndEvidence[1])
parseOut(queryAndEvidence[0],queryAndEvidence[1],result,args.bn)

# Print time until completion
print("--- %s seconds ---" % (time.time() - start_time))