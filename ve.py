import sys
from parser import *
import itertools

def varElim(bn,query,evidences):

	# TEST ZONE #
	#evidences = [['MaryCalls','t'],['JohnCalls','t']]

	# --------- #

	factors=initFactors(bn)
	
	nHidden = [str(query)]
	for x in evidences:
		nHidden.append(str(x[0]))


	for var in bn.getNodes():
		factors = makeFactor(var,evidences,factors)
		if str(var) not in nHidden:
			factors = sumOut(var, factors)

	return normalize(bn,query,evidences,factors)

def normalize(bn,query,evidences,factors):
	""" Eg:
		query: var name, like 'Burglary'
		bn: Grafo
		evidence: [['MaryCalls','t'],['JohnCalls','t']]
		factors: [['Burglary', 't,f'],{'t': P1, 'f': P2}] """

	# Watch possible values for query
	Qvalue = bn.infoNode(query)['values'][0]
	
	containedIndex = float("inf")
	for i, var in enumerate(factors[0][0]):
		if var[0] == str(query):
			containedIndex = i
			break

	for key, value in factors[0][1].items() : 
		Key = key.split(",")
		if Key[containedIndex] == 't':
			PfactorQvalueV = value
		else:
			PfactorQvalueF = value

	# returns a list with [prob , value, prob, ~value]
	return ['t', PfactorQvalueV/(PfactorQvalueF+PfactorQvalueV),'f', PfactorQvalueF/(PfactorQvalueF+PfactorQvalueV)]

def getProb(bn,var):
	""" getProb shall receive a BN and a var in the following format: ['var name','state']. """

	# Fetch cpt and analyze size
	table = bn.infoNode(var[0])['cpt']
	tableRows = len(table)
	tableColumns = len(table[0])

	# Init probability var.
	P = 0

	# Check if table has minimum dimensions
	if tableRows <= 1:
		print("Table is wrong! Exiting.")
		sys.exit(-1)

	# Calculate probability
	for row in table:
		if row[0] == var[1]:
			P = P+row[tableColumns-1]

	return P

def makeFactor(var,e,factors):
	"""
	var ->
	e ->
	factors ->
	"""

	variables = []
	possibleValues = {}
	containedIndex = 0
	funtionsToFactor = []

	#Find the necessary factors here var is present
	for function in factors:
		for testvar in function[0]:
			if(testvar[0] == var):
				funtionsToFactor.append(containedIndex)
		containedIndex += 1

	#Find the variables that the new factor will be composed by
	for val in funtionsToFactor:
		for testvar in factors[val][0]:
			if [testvar[0],testvar[1].split(",")] not in variables:
				variables.append([testvar[0],testvar[1].split(",")])

	removeEvidence = []
	i=j=0
	for var in e: 
		j=0
		for var2 in variables:
			if var[0] == var2[0]:
				removeEvidence.append([j,var[1]])
			j+=1
		i+=1
	
	#Generate permutations
	perms = set()
	for comb in itertools.combinations_with_replacement("tf",len(variables)):
		for perm in itertools.permutations(comb,len(variables)):
			perms.add(perm)
	#Calculate values
	perms = list(perms)

	newDictFactor = {}
	
	for combo in perms:
		#print combo
		totalPerline = 1
		for val in funtionsToFactor:
			order = []
			for testvar in factors[val][0]:
				order.append(variables.index([testvar[0],testvar[1].split(",")]))
			comboSet = [combo[i] for i in order]

			if(",".join(comboSet) in factors[val][1]):
				totalPerline *=  factors[val][1][",".join(comboSet)]
		#print totalPerline
		flag = 0
		for rem in removeEvidence:
			if combo[rem[0]] != rem[1]:
				flag=1
		if flag == 0:
			newDictFactor[",".join(combo)]=totalPerline

	#Generate out format
	for item in variables:
		item[1] = ",".join(item[1])

	newFunction = [variables,newDictFactor]

	factors = [i for j, i in enumerate(factors) if j not in funtionsToFactor]
	factors.append(newFunction)
	return factors
		


def initFactors(bn):

	# init factors list
	factors = []
	variables = bn.getNodes()

	for i, var in enumerate(variables):
		
		# start a new factor
		factor = []

		# init factor content: variables and table
		Ftable = {}
		varList = []

		# Append to varList info regarding the selected variable
		varList.append([str(var),",".join(bn.infoNode(str(var))['values'])])

		# fetch parents data
		if 'parents' in bn.infoNode(var):
			parents = bn.infoNode(var)['parents']
			
			# build a list with parent name and values and append to variables list
			for parent in parents:
				value = bn.infoNode(str(parent))['values']
				value = ",".join(value)
				varList.append([str(parent),value])


		# fill table with ordered cpt
		table = bn.infoNode(var)['cpt']
		for row in table:
			Ftable[",".join(row[:-1])] = row[-1]

		# Append all elements to factor, and add factor to list of factors
		factor.append(varList)
		factor.append(Ftable)
		factors.append(factor)

	return factors


def sumOut(var, factors):

	variables = []
	newDictFactor = {}
	index = None
	funtionsToFactor = []
	
	containedIndex = 0
	for function in factors:
		for testvar in function[0]:
			if(testvar[0] == var):
				functionToSum = containedIndex
		containedIndex += 1


	i = 0
	for testvar in factors[functionToSum][0]:
			if [testvar[0],testvar[1].split(",")] not in variables:
				variables.append([testvar[0],testvar[1].split(",")])
				if testvar[0] == var:
					index = i
				i +=1

	newVariables = [item for item in variables if item[0] != var]

	#Generate permutations
	perms = set()
	for comb in itertools.combinations_with_replacement("tf",len(newVariables)):
		for perm in itertools.permutations(comb,len(newVariables)):
			perms.add(perm)

	#Calculate values
	perms = list(perms)
	for perm in perms:
		newDictFactor[",".join(perm)] = 0


	for item in factors[functionToSum][1]:
		value = item.split(",")
		value.pop(index)
		newDictFactor[",".join(value)] +=factors[functionToSum][1][item]

	new_data = {k: v for k, v in newDictFactor.items() if v != 0}
	newDictFactor = new_data
	
	for item in newVariables:
		item[1] = ",".join(item[1])
	factors.pop(functionToSum)
	factors.append([newVariables, newDictFactor])
	return factors

#a = [[[["MaryCalls","T,F"],["Alarm","T,F,H"]], {"T,T":0.9 , "T,F":0.8, "T,H":0.7, "F,T":0.9 , "F,F":0.8, "F,H":0.7 }]]
#
#variables = []
#possibleValues = {}
#for function in a:
#	for var in function[0]:
#		print var
#		variables.append(var[0])
#		possibleValues[var[0]]=var[1]
#
#print variables	
#print possibleValues["Alarm"]
#	[variavies , dicionario ]

