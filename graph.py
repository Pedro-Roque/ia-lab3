import networkx as nx
import matplotlib.pyplot as plt

class graphConstructor:

	def __init__(self):
		self.Grafo = nx.DiGraph()

	def addNode(self,node,alias=None,cpt=None,values=None,parents=None):
		dic = {}
		if alias != None:
			dic['alias'] = alias
		if cpt != None:
			dic['cpt'] = cpt
		if values != None:
			dic['values'] = values
		if parents != None:
			Par = []
			for parent in parents:
				if parent not in self.Grafo.nodes():
					for Node in self.Grafo.nodes():
						if parent == self.Grafo.node[str(Node)]['alias']:
							Par.append(str(Node))
				else:
					Par.append(str(parent))
			dic['parents'] = Par
		
		self.Grafo.add_node(node,dic)

	def infoNode(self,Node):
		return self.Grafo.node[Node]

	def addEdge(self,nodeFrom,nodeTo):
		# What if edge is between two nodes that do not exist? Alias can be only 2 letters?
		#print("Adding Edge. Nodes:",self.Grafo.nodes())
		if nodeFrom not in self.Grafo.nodes():
			for Node in self.Grafo.nodes():
				if nodeFrom == self.Grafo.node[str(Node)]['alias']:
					#print("IN ADD EDGE:",self.Grafo.node[Node])
					self.Grafo.add_edge(str(Node), nodeTo)
		else:
			self.Grafo.add_edge(nodeFrom, nodeTo)

	def editEdge(self,nodeFrom,nodeTo,edgeTransport,edgeTime,edgeCost,edgeTf,edgeTi,edgePeriod):
		""" Nos queremos passar uma arsesta de autocarro para camiao, ou so queremos eliminar essa e fazer outra? """
		self.Grafo.add_edge(int(nodeFrom), int(nodeTo), transport=edgeTransport, time=int(edgeTime), cost=int(edgeCost), Tf=int(edgeTf), Ti=int(edgeTi), P=int(edgePeriod))

	def deleteEdge(self,nodeFrom,nodeTo,receiptKey):
		self.Grafo.remove_edge(int(nodeFrom),int(nodeTo),key=receiptKey)

	def printGraph(self,item):
		"""Ainda nao funciona com duas arestas entre os mesmos nos, mas hei-de perceber porque. De qualquer maneira, e so para ser bonito, mas pode ser alterado"""
		pos = nx.spring_layout(self.Grafo)
		nx.draw(self.Grafo, pos)
		edge_labels=dict([((u,v,),d[item])
		for u,v,d in self.Grafo.edges(data=True)])
		nx.draw_networkx_edge_labels(self.Grafo, pos, edge_labels=edge_labels, label_pos=0.3, font_size=7)
		plt.show()
		#plt.savefig('foo.png')

	def numberOfEdges(self,nodeFrom=None,nodeTo=None):
		if nodeFrom == None:
			print(self.Grafo.number_of_edges())
		else:
			print(self.Grafo.number_of_edges(nodeFrom,nodeTo))

	def getAllEdges(self,node=None,in_data=False,in_keys=True):
		return self.Grafo.edges(node,data=in_data,keys=in_keys)

	def getEdges(self,nodeFrom=None,nodeTo=None,key=None,in_data=False,in_keys=True):
		outList = []
		for i,val in enumerate(self.getAllEdges(nodeFrom, in_data)): 
			#print val[0],val[1],val[2]
			if key == None and val[1] == nodeTo:
				outList.append(val)
			elif val[2] == key and val[1] == nodeTo:
				outList.append(val)

		return outList
		
	def getNodes(self):
		return self.Grafo.nodes()

	def getNodeNeighbors(self,node):
		return nx.DiGraph.neighbors(self.Grafo,node)





	