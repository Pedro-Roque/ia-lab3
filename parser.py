import logging
import os
import itertools

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]

def solve(lis):                                        
	for x in lis:
		try:
			yield float(x)
		except ValueError:
			yield x   
			pass

def iter(a):
	for i in range(0,len(a)):
		yield a[i]


def parseBN(file):
	"""
	File parser
	Inputs:
		file -> File to be parsed
	Outputs:
			"""
	subFileBlocks = []
	if not file.endswith(".bn"):
		logging.error("Wrong File Extension")
		return []
	try:
		FILE = open(file)
	except IOError:
		logging.error("Could Not Open File")

	for line in FILE:
		# This is a comment (That is for the week mind of humans just IGNORE! Oh whai I'm also a comment, NOOOOOO)
		if line[0] == '#':
			continue

		#Strip That mean \n
		line = line.strip('\n')
		
		if line == "":
			continue

		if line.startswith('VAR') or line.startswith('CPT'):
			subFileBlocks.append([line])
		else:
			subFileBlocks[-1].append(line)

	VAR = []
	CPT = []
	for blocks in subFileBlocks:
		client = {}
		for data in blocks:
			if data == "VAR" or data == "CPT":
				continue
			
			dataBunch = data.split()
			
			if blocks[0] == "VAR":
				if dataBunch[0] == "alias" or dataBunch[0] == "name":
					client[dataBunch[0]] =  dataBunch[1]
				else:
					client[dataBunch[0]] =  dataBunch[1:]
			
			if blocks[0] == "CPT":
				if dataBunch[0] == "var":
					client[dataBunch[0]] =  dataBunch[1]
				elif dataBunch[0] == "table":
					client[dataBunch[0]] =  list(solve(dataBunch[1:]))
				else:
					client["table"].extend(list(solve(dataBunch[0:])))

		if blocks[0] == "VAR":
			VAR.append(client)

		if blocks[0] == "CPT":
			CPT.append(client)

	for var in CPT:
		var["table"] = list(chunks(var["table"],next((i for i, x in enumerate(var["table"]) if "float" in str(type(x))), len(var["table"]))+1))

	return [CPT,VAR]


def parseIN(file):
	"""

	"""

	if not file.endswith(".in"):
		logging.error("Wrong File Extension")
		return []
	try:
		FILE = open(file)
	except IOError:
		logging.error("Could Not Open File")


	query = ""
	evidance = []
	for line in FILE:
		# This is a comment (That is for the week mind of humans just IGNORE! Oh whai I'm also a comment, NOOOOOO)
		if line[0] == '#':
			continue

		if line.startswith('QUERY'):
			line = line.strip('\n')
			data = line.split()
			query = data[1]

		elif line.startswith('EVIDENCE'):
			line = line.strip('\n')
			data = line.split()
			evidance = list(chunks(data,2))
			evidance.pop(0)
		else:
			pass

	for ev in evidance:
		ev[1] = ev[1].lower()
		
	return [query,evidance]

def parseOut(query,evidance,result,filename):
	"""
	query->Sting
	evidance->[["Var","value"]...,["Var","value"]]
	result->
	"""

	filename = filename[:filename.rfind(".")+1] + "sol"
	print(filename)
	result[0] = result[0].upper()
	result[1] = str(result[1])
	result[2] = result[2].upper()
	result[3] = str(result[3])


	OUTFILE = open(filename,"w")
	OUTFILE.write("########## SOLUTION ##########\n")
	OUTFILE.write("QUERY "+query)
	OUTFILE.write("\nEVIDENCE " + " ".join(" ".join(map(str,l)) for l in evidance))
	OUTFILE.write("\nQUERY_DIST " + " ".join(result))